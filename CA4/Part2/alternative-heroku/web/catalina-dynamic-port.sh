#!/bin/bash
echo $PORT
sed -i 's/port="8080"/port="'${PORT}'"/' /usr/local/tomcat/conf/server.xml
export JAVA_OPTS="-Djava.awt.headless=true -server -Xms48m -Xmx512m -XX:MaxPermSize=512m"
/usr/local/tomcat/bin/catalina.sh run

