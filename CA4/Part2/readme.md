DEVOPS 2021-2022
=

CA4 PART2 - CONTAINERIZATION
-
**Salomé Pereira - 1211795@isep.ipp.pt**

---------------------------------------------------------
Creating Containers with Docker Compose
=

For this assignment, we are going to set up a containerized environment that will
consist of two containers:

1. A container to run the application developed for CA3-Part2 on a Tomcat server.
2. A container to run the database.

These containers will be connected in same network, which we will configure.


1.Creating db image:
-
The image for the db container was created with the Dockerfile available at
https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

2.Creating web image:
-
The Dockerfile for this image was based on the Dockerfile available at
https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

**Steps:**
1. To support Java 11, that we used to develop our app, 
the image will be built from **tomcat:9.0.48-jdk11-openjdk-slim**;

2. We install the required dependencies;

3. We create a temporary folder and clone our own repository into it;

4. We will change to the root folder of our app, grant execute 
permissions over the build.gradle file, 
and build our application by running the command `./gradlew clean build`;

5. We copy the built .war file into the Tomcat server directory;

6. We remove our repository's clone;

7. We expose Tomcat's default port, **8080** where our app will be running.

**Result:**

~~~dockerfile
# Step 1
FROM tomcat:9.0.48-jdk11-openjdk-slim

#Step 2
RUN apt-get update -y
RUN apt-get install -f
RUN apt-get install git -y
RUN apt-get install nodejs -y
RUN apt-get install npm -y
RUN mkdir -p /tmp/build

#Step 3
WORKDIR /tmp/build/
RUN git clone https://salome_pereira@bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git

#Step 4
WORKDIR /tmp/build/devops-21-22-atb-1211795/CA4/Part2/tut-basic-gradle/react-and-spring-data-rest-basic
RUN chmod u+x gradlew
RUN ./gradlew clean build

#Step 5
RUN cp build/libs/basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

#Step 6
WORKDIR /tmp/
RUN rm -R -f build

#Step 7
EXPOSE 8080
~~~
3.Docker Compose
-

This docker-compose.yml file was based on the docker-compose.yml available at
https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/

Our containerized environment is defined in the **docker-compose.yml** file.
Among other specifications, we configure a bridge network for the two containers we will create, so they can talk to each other, the host and access the internet.

**Steps:**
1. We declare the services: **web** and **db**;
2. We indicate the relative path to the directory where the 
Dockerfiles for building the images are located;
3. We create the network;
4. We define the IP addresses of the containers, so they are connected in the same network;
5. We map the containers' exposed ports to the host's ports:   
**- "host-port:container-port"**;
6. In order to function, the web container needs db to be up. 
We declare the dependency from web to db, so that the db container is created first. 
7. We define a **volume**, i.e. a directory in the container and a directory in the host whose contents are synchronized:  
**- host-dir:container-dir**

 **Result:**
~~~yml
version: '3'

services:
  #Step 1
  web:
    #Step 2
    build: web
    #Step 5
    ports:
      - "8080:8080"
    #Step 4
    networks:
      default:
        ipv4_address: 192.168.33.10
    #Step 6
    depends_on:
      - "db"
  #Step 1
  db:
    #Step 2
    build: db
    #Step 5
    ports:
      - "8082:8082"
      - "9092:9092"
    #Step 7
    volumes:
      - ./data:/usr/src/data-backup
    #Step 4
    networks:
      default:
        ipv4_address: 192.168.33.11
#Step 3
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.33.0/24
~~~

### Important Note:
Before we build the images, 
we must ensure the IP address of the db service in the **application-properties** file 
is correct. It must be the same as the one we specify in docker-compose.yml.

~~~properties
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
~~~

4.Up, up and away!
-

When the Dockerfiles and the docker-compose.yml file are ready, 
we can start our docker composition by opening a terminal 
in the directory where our docker-compose.yml is located and running :
~~~shell
$ docker-compose up
~~~
This will create the images, start the containers and create the network.
Now we can access our application at: 

http://localhost:8080/basic-0.0.1-SNAPSHOT

**It works!**
![img.png](img.png)


5.Obtaining a copy of the database file
-
If we copy the database file, **jpadb.mv.db**, into the container's shared folder, it automatically appears
in our host machine's folder specified in the docker-compose.yml file.
In order to achieve this, when the container is up and running, we initiate a shell session in the container:

~~~shell
$ winpty docker-compose exec db bash
~~~ 
When we are "in" the container, we run:
~~~shell
$ cp /usr/src/app/jpadb.mv.db /usr/src/data-backup
~~~
Finally:
~~~shell
$ exit
~~~

**Result:**

![img_2.png](img_2.png)
![img_3.png](img_3.png)

**It worked!**

6.Publishing images to Docker Hub
-
As we have seen in the previous report, we have to tag the images we want to publish
including our Docker Hub username followed by a slash.

We follow these steps:
~~~shell
#Step 1
$ docker login

#Step 2
$ docker tag part2_db salomepereira1211795/db:v1
$ docker tag part2_web salomepereira1211795/web:v1

#Step 3
$ docker push salomepereira1211795/db:v1
$ docker push salomepereira1211795/web:v1

~~~

--------

Going Further - Deploying Cointainers to Heroku
=
*"Heroku is a platform as a service (PaaS) that enables developers to build, 
run, and operate applications entirely in the cloud."*  
https://www.heroku.com/

1.Setup
-
Our goal is to deploy the application to the cloud. To follow along with the described steps, it is necessary to:
1. Register on Heroku: https://signup.heroku.com/
2. Install the Heroku CLI: https://devcenter.heroku.com/categories/command-line

2.Building the Container
-
While in local development, we might need to use a composition with multiple containers. 
When deploying to Heroku, this might not be necessary since
Heroku provides multiple add-ons, for instance, databases. 
However, there is no Heroku add-on for an H2 database.   
(See: https://devcenter.heroku.com/articles/local-development-with-docker-compose)

When trying to deploy two containers, we encountered the following difficulty: 
**Heroku always allocates IP addresses and ports dynamically.**  
This means that we cannot specify the IP address and port of the db service in the application.properties file, because it changes every time the application boots. 
So, for simplicity, we will deploy a single container to run the application from CA3-Part2, 
with an in-memory H2 database. 

1. First, we change the application.properties file:
~~~properties
spring.datasource.url=jdbc:h2:mem:jpadb
~~~

3. Next, we will create a Dockerfile to build our image.
We will copy the .war folder we built in the previous step
from our host machine "into" the image. This approach is preferable because it is simpler,
requiring fewer Dockerfile commands, reducing the possibility of error
resulting in a lighter image, with fewer files and installed dependencies. 


To build our app, we run:

~~~shell
$ ./gradlew clean build
~~~
Now that we have our .war file, we place it in the same directory as our Dockerfile.

### **NOTE:**  The following Heroku specifications that must be taken into account: 

   1. According to Heroku documentation, the EXPOSE Dockerfile command is not supported:

> While EXPOSE can be used for local testing, it is not supported in Heroku’s container runtime.
> Instead your web process/code should get the $PORT environment variable.
> 
> https://devcenter.heroku.com/articles/container-registry-and-runtime#unsupported-dockerfile-commands

We cannot assume that the Tomcat server will execute on port 8080, 
because Heroku dinamically allocates a port to which the web app
must bind. 
 
2. We must limit the memory used by th JVM to avoid the following error:

~~~shell
2022-05-23T13:18:33.776687+00:00 heroku[web.1]: Process running mem=574M(112.2%)
2022-05-23T13:18:33.796035+00:00 heroku[web.1]: Error R14 (Memory quota exceeded)
~~~

To resolve these issues, we add the following BASH script to our image:

~~~shell
#!/bin/bash
echo $PORT
sed -i 's/port="8080"/port="'${PORT}'"/' /usr/local/tomcat/conf/server.xml
export JAVA_OPTS="-Djava.awt.headless=true -server -Xms48m -Xmx512m -XX:MaxPermSize=512m"
/usr/local/tomcat/bin/catalina.sh run
~~~
The **sed** command will edit the **server.xml** file, replacing port 8080 with the port defined by Heroku.  
The fourth line will limit the memory used by Tomcat, so as not to exceed the maximum quota of 512 MB available on free Heroku.  
Then the script will execute catalina and boot the server.  

We create a file containing this script, place it in the same directory of our Dockerfile, and in the Dockerfile we use the COPY command to copy the script into **/usr/local/tomcat/bin/**
The script is then given execute permissions, and we use the CMD command to run it when the container is instantiated.


**Result:**

~~~dockerfile
FROM tomcat:8-jdk11-temurin

RUN apt-get update -y
RUN apt-get install sudo nano git nodejs npm -f -y
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

COPY ./basic-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

ADD ./catalina-dynamic-port.sh /usr/local/tomcat/bin/catalina-dynamic-port.sh
RUN chmod a+x /usr/local/tomcat/bin/catalina-dynamic-port.sh

CMD ["catalina-dynamic-port.sh"]

~~~


3.Deploying the container
-

To deploy our container, we follow these steps:

1. Log into Heroku, inserting our account credentials after running the command:
~~~shell
$ heroku login -i
~~~
2. Then, run:
~~~shell
$ heroku container:login
~~~
3. We create a new app:
~~~shell
$ heroku create devops-ca4-part2

Creating devops-ca4-part2... done
https://devops-ca4-part2.herokuapp.com/ | https://git.heroku.com/devops-ca4-part2.git
~~~
4. In the directory that contains our Dockerfile, we run:
~~~shell
$ heroku container:push web --app devops-ca4-part2
~~~
**web** is the name of the service. This will build the container from the image and push it to the Heroku Container Registry.

~~~shell
$ heroku container:release web --app devops-ca4-part2
~~~

We can view the logs to understand how the process is going with this command:
~~~shell
$ heroku logs --tail -a devops-ca4-part2
~~~

**It works!**

![img_4.png](img_4.png)

The app is available at:
https://devops-ca4-part2.herokuapp.com/basic-0.0.1-SNAPSHOT/