DEVOPS 2021-2022
=

CA4 PART1 - CONTAINERIZATION
-
**Salomé Pereira - 1211795@isep.ipp.pt**

---------------------------------------------------------
Creating Containers with Docker
-
Setup
-
The goal of this class assignment is to build containers to run a chat server,
so that we can connect chat clients to it from our host machine.
We will develop two versions:

- In the first approch, we will build the chat server when we create the image.  
  The Dockerfile for this version is available inside the folder **v1**.  
  The corresponding image is **salomepereira1211795/my-image** , available
  at: https://hub.docker.com/repository/docker/salomepereira1211795/my-image


- In the second approach, we will build the chat server in our host computer and copy the
  built .jar file into the image.  
  The Dockerfile for this version is available inside the folder **v2**.
  The corresponding image is **salomepereira1211795/my-image2**, available at:
  https://hub.docker.com/repository/docker/salomepereira1211795/my-image2

Our images will be based on an exising image, **ubuntu**.

These were the steps followed to attain the requirements:

1. Writing Dockerfiles;
2. Building images;
3. Running containers;
4. Testing containers;
5. Publishing images in a repository.

1.Dockerfiles
-

### First Version

Steps:
1. Of course, it is necessary to install Java. Git is needed as well, since we want to clone a repository;

2. We clone our repository;

3. We change the working directory to the root of our project;

4. We grant execute permission over the gradle wrapper file;

5. We build our application;

6. When executed, the gradle task runServer(developed for CA2-Part1) launches the server at port 59001, so this is the port of the container we will expose.

7. Finally, when a container is created from this image (and only then), we want it to run the gradle task runServer. 

**Result:**
~~~dockerfile
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-8-jdk-headless
RUN apt-get install -y git

RUN git clone https://salome_pereira@bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git

WORKDIR devops-21-22-atb-1211795/CA2/Part1/luisnogueira-gradle_basic_demo-8b7abb1a82e8

RUN chmod u+x gradlew
RUN ./gradlew clean build

EXPOSE 59001

ENTRYPOINT ./gradlew runServer
~~~


### Second Version

1. Since we compiled the application in our host with Java 11, 
this is the version we have to install in the image.
2. After compiling the application in our host, we place the .jar file in the same directory as the Dockerfile.
We copy this .jar into the image, with the same name.
3. We will lauch the server at port 59001, so this is the port we will expose
4. We specify the command to be run by the containers instantiated from this image (this command to run the server was retrieved from the Chat Project documentation).

**Result:**
~~~dockerfile
FROM ubuntu

RUN apt-get update -y
RUN apt-get install -y openjdk-11-jdk-headless

COPY basic_demo-0.1.0.jar basic_demo-0.1.0.jar

EXPOSE 59001

ENTRYPOINT java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
~~~

2.Building images and some other useful command examples
-
After writing our Dockerfiles, **in the directory where they are located**,
we use the following command to build the images:

~~~shell
$ docker build -t my-image .
~~~
The size of the resulting image is **777MB**.
~~~shell
$ docker build -t my-image2 .
~~~
The size of the resulting image is **592MB**, quite lighter than the first.

Sometimes, it is useful to ignore cache, for example, 
when something went wrong with the build for the first time 
or if we made changes to the Dockerfile:

~~~shell
$ docker build --no-cache -t my-image2 .
~~~

To view all images locally available:

~~~shell
$ docker images
~~~

To remove an image:

~~~shell
$ docker rmi my-image
~~~

3.Building containers and some other useful command examples
-
To build containers based on the images we created, we use the command:

~~~shell
$ docker run -p  59001:59001 --name my-container -d my-image:latest
~~~

~~~shell
$ docker run -p  59001:59001 --name my-container2 -d my-image2:latest
~~~
**We are forwarding the container's 59001 port (rightmost number) to the host's 59001 (leftmost number) port.**
This is important, because when we launch a client on our host by running the runClient gradle task,
the task assumes the chat server's IP is localhost and its port is 59001.

**Note: they can't be running at the same time, because they would occupy the same port on the host machine.**

To view all containers:

~~~shell
$ docker ps -a
~~~

To stop a running container:

~~~shell
$ docker stop my-container
~~~


To remove a container:

~~~shell
$ docker rm my-container
~~~

4.Experimenting with the containers and results
-
As mentioned above, the chat clients will connect to a chat server at localhost:59001.
As we will see, both containers work!

### First Version

![img_1.png](img_1.png)

![img_2.png](img_2.png)

### Second Version
![img_3.png](img_3.png)

![img_4.png](img_4.png)





5.Pushing Images to the repository
-
Now that our images are developed, we can publish them!
To publish our images to a Docker Hub repository, we have to create an account.  
Then, we have to log in with our credentials using the command:

~~~shell
$ docker login
~~~

So that our images are accepted in the repository,
We have to tag them incluiding our username followed by a slash:

~~~shell
$ docker tag my-image salomepereira1211795/my-image
~~~

~~~shell
$ docker tag my-image2 salomepereira1211795/my-image2
~~~

Finally, we can push the images:

~~~shell
$ docker push salomepereira1211795/my-image
~~~

~~~shell
$ docker push salomepereira1211795/my-image2
~~~

The process may take a while...
**Here they are:**

![img.png](img.png)

To log out:

~~~shell
$ docker logout
~~~