DEVOPS 2021-2022
=

CA3 PART1- VIRTUALIZATION
-
**Salomé Pereira - 1211795@isep.ipp.pt**

---------------------------------------------------------
Virtualization using Virtual Box
 =

1.Setting up the Virtual Machine
-
Virtualization is widely used to create web servers and allows taking the most of the hardware, 
by creating software simulated computers. 
Some advantages of simulated computers are:
- Many VMs can run over a physical machine, reducing hardware cost (and other associated costs).
- Mantainance of VMs is made trough software.
- It is possible to make an exact copy of a VM.
- Physical hardware may be allocated according to actual needs.


### 1.1 Hypervisor

To start our work, we need to install a **Hypervisor**: we will be using **VirtualBox**
This is a kind of software that  simulates some hardware (disks, video displays) our machine needs,
such as disks for storage or a video display,
and allows access to the physical hardware (the host machine's) that is not simulated
for practical reasons, such as CPU and RAM
(simulating these hardware components would result in a very slow virtual machine).

### 1.2 Creating a VM
To create our virtual machine, we followed the steps described 
in the provided documents and demonstrated in the class except we installed java 11 instead of 8.
The operating system we installed in the VM does not have a graphic user interface, it has to be managed through a console
This impses us some limiations, however, it comes with some advantages as well:  
CPU and memory resources, that would be used
to display graphics are available to improve the machine’s performance. 
If the intent is to use the VM as a web server, a graphic user interface may not be as important.


### 1.3 Networking
In order to complete this assignment, it is important to configure network connections properly: 

In order to proceed with the installation of the Ubuntu 18.04 OS, internet connection is necessary.
It is also necessary to create a network connection between our VM and our host, or else they cannot communicate.

Installing an SSH server enables the possibility of SSH connection, easing certain tasks: we can log into our machine to manage it remotely 
(for example from a terminal in our host).

Finally, installing an FTP server allows us to transfer files into it or from it 
using an FTP client such as Filezilla.

2.Cloning repository into the VM
-

Now thar our SSH connection is set up, we can log into our virtual machine 
using a shell in our host with the command:

~~~ shell
$ ssh salome@192.168.56.5
~~~

Having access to a terminal session allows us to control the machine.
Let's clone our DevOps repository:

~~~ shell
$ git clone https://salome_pereira@bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git
~~~
Now, the repository is in our home directory.

3.Building and Executing the CA1 Spring Boot Tutorial Basic Project
-
The use of a wrapper file allows us not to build and execute our applications without having to
install the build tools in our machine.  

1. First, to execute our project, it is necessary to grant ourselves execute permissions 
over the maven wrapper file. In our project directory, we run:
~~~ shell
$ chmod u+x mvnw
~~~
2. Then, 
~~~
$ ./mvnw spring-boot:run
~~~
We won't be able to view the application, because our VM's operating system does not
provide a graphic environment. 

If we try to, using **curl**, for instance, we get some html code in our console,
with **lynx**, we get a blank page.

However, since our virtual machine has a network connection to the host machine
**we can access it using a browser in our host machine**.

3. Now in a browser in our host machine,
we type the IP address we configured for our virtual machine, 
followed by the port where the application is running:

http://192.168.56.5:8080/

**Result:**
![](firstapp.png)
**It works!**  
4. When we are done with our application, we type **CTRL + C** in our shell to stop it.

4.Building and executing the CA2-Part1 gradle_basic_demo Project
-
1. Again, we have to grant ourselves execute permission 
over the gradlew file:
~~~ shell
$ chmod u+x gradlew
~~~
2. Now, let's run the task we created in the first part of the second assignment to execute the server:
~~~ shell
$ ./gradlew runServer 
~~~
**Output:**
~~~ 
> Task :compileJava UP-TO-DATE
> Task :processResources UP-TO-DATE
> Task :classes UP-TO-DATE

> Task :runServer
The chat server is running...
~~~
Everything seems to be working fine!

3. As we have mentioned earlier, we don't have a graphic environment. 
What happens if we run the server in background and try to run the clients?

~~~ shell
$ ./gradlew runServer &
~~~

Output:
~~~ shell
[1] 1254
~~~
Now, the client:
~~~ shell
$ ./gradlew runClient
~~~
The build fails. Part of the output message is explainatory:

~~~ 
> Task :runClient FAILED
Exception in thread "main" java.awt.HeadlessException:
No X11 DISPLAY variable was set, but this program performed an operation which requires it.
~~~

4. Let's bring the task to the foreground:
~~~ shell
$ fg 1
~~~

5. Now, let's try to run the clients **in our host machine**:
~~~ shell
$ ./gradlew runClient
~~~

**It fails! Why?**

~~~
> Task :runClient
java.net.ConnectException: Connection refused: connect
at java.base/java.net.PlainSocketImpl.connect0(Native Method)
at java.base/java.net.PlainSocketImpl.socketConnect(PlainSocketImpl.java:101)
~~~

The runClient task is defined to connect the clients to **localhost:59001**
But now, the server is not running in our host machine, 
it is running in the virtual machine, at port 59001.

**How do we fix this?**

6. In our host machine, we can run the command:
~~~ shell
$ java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatClientApp 192.168.56.5 59001
~~~
Or we can add a new gradle task to execute, for example:
~~~ groovy
 task runClientServerInVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on a VM at 192.168.56.5:59001"

    classpath = sourceSets.main.runtimeClasspath

    mainClass = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
}

~~~
**Result:**

![](secondapp.png)

**It works!**


5.Running other tasks
-

We ran the other tasks of the four projects 
already developed in previous assignments, to ensure they work well on our VM.
Here is the summary:


| 	CA2 Part 1    |                    	                     | 	  |  
|----------------|:----------------------------------------:|----|
| ./gradlew copy | Backup directory is created with content | OK |
 | ./gradlew zip  |  Zip directory is created with content   | OK |

### CA2 Part2  


| 	CA2 Part 2 - Gradle   |                              	                              | 	  |  
|------------------------|:-----------------------------------------------------------:|----|
 | ./gradlew build        |                      Project is built                       | OK |
| ./gradlew bootRun*     |             Application runs (see image below)              | OK |  
| ./gradlew cleanWebpack |          Deletes src/resources/main/static/built/           | OK |
| ./gradlew clean 	      | Deletes build directory  + src/resources/main/static/built/ | OK |
| ./gradlew copyJars     |             Dist folder is created with content             | OK |

![](/app3.png)
 *After running clean or cleanWebpack, it is necessary to build the project again before running in order to
generate the frontend again.

| CA2 Part2 - Maven (Alternative) |                                                      |     | 
|---------------------------------|:----------------------------------------------------:|-----|
| ./mvnw clean                    | Target folder and webpack generated files are erased | OK  |
| ./mvnw package -P dist          |         Dist folder is created with content          | OK  |  
|./mvnw spring-boot:run           |          Application runs (see image below)          | OK  |  

![](/app4.png)

Conclusion
=

Apart from the tasks that require a graphic environment,
in what respects to executing our applications, we were able to replicate in
our virtual machine everything we can do in the host machine.
With a virtual machine, we have built an enclosed environment 
where we can experiment with our applications.
The network connections we implemented allow our VM to connect 
to the Internet and  enable communication between the VM and host. 
The VM is dependent on the host but, in many ways, is a separate, independent machine from its host.


