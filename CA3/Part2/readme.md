DEVOPS 2021-2022
=

CA3 PART2- VIRTUALIZATION
-
**Salomé Pereira - 1211795@isep.ipp.pt**

---------------------------------------------------------
Virtualization Using Vagrant and VirtualBox
=

1.Initial Setup
-
Vagrant is an open source software solution to create and maintain
virtual development environments in an automated way, enabling their portability and simplifying the
management and configuration of Virtual Machines.

These configurations are described in a text file, the **Vagrantfile**.

Vagrant makes use of preconfigured **boxes**, which are images of VMs with an operating system.
There is a public catalog of available Vagrant boxes
(https://app.vagrantup.com/boxes/search), and it is also possible to make a custom box.

To create virtual machines, Vagrant resorts to hypervisors - **Providers**.

Several guest Virtual Machines may be configured in a single Vagrantfile,
establishing a **multi-machine environment**.
In the course of this exercise, we will set up and provision two Virtual Machines:

1. One to run the Spring Boot application from CA2-Part2, called **web**
2. The other to run our database server, called **db**.

The provider we will be using is **VirtualBox**.

We will use the provided example's Vagrantfile
(from https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/)
as the basis of our work,
adapting it to our needs, and we will follow the steps described at
https://bitbucket.org/atb/tut-basic-gradle/src/master/.

In relation to the provided Vagrantfile example, some changes are needed:

- Since we used Java 11 to develop our application,
  this is the version we have to install in our Virtual Machines.


- The box **envimation/ubuntu-xenial**, from the provided Vagrantfile example, does not support Java 11.
  We have to use a different one: we elected **bento/ubuntu-18.04**, since it works well for our purposes.

> **NOTE**: the Vagrantfile of the CA3-Part2 can be found at CA3/Part2/class-exercise
> and the alternative at CA3/Part2/alternative.
> **We made two copies of the work developed for CA2-Part2**,
> so we can adapt the configurations to our needs.

2.Update the Vagrantfile, so it uses the Gradle version of the Spring Boot Application
-
We want to run our own application, the one we worked on CA2-Part2.
So, in the provision section of the web VM of the Vagrantfile:

1. We introduce the link to clone our repository (second line).
2. Then, we instruct the shell to change to the directory where the gradle version of the spring boot application
   is located (third line).
3. We grant the execution permission to the gradle wrapper file (fourth line).
4. We execute the gradle tasks **clean** and **build** (fifth line).
5. Since we are doing a trial-and error process, it is useful to add a command before `git clone`
   to clean up outdated versions of our repository (first line).

Result:

~~~ shell
      rm -R -f devops-21-22-atb-1211795
      git clone https://salome_pereira@bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git
      cd devops-21-22-atb-1211795/CA3/Part2/tut-basic-gradle/react-and-spring-data-rest-basic
      chmod u+x gradlew
      ./gradlew clean build
    
~~~

**Now, it is necessary to make some changes to our application**

2.Adding support for .war builds
-

To deploy our app on the Tomcat8 server,
the compilation process of our app needs to
generate a **.war** file.
When we build our app, we are generating **.jar** files
To obtain a .war file, we must make the following changes to our application:

1. We must add the plugin .war to our build.gradle.

~~~groovy
id 'war'
~~~

2. We must also add a new dependency to our build.gradle file, with **providedRuntime** scope:

~~~groovy
providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
~~~

3. We must add a new Java class, **ServletInitializer** to our application
   (We retrieved it as is from the tut-basic-gradle example repository).


4. In the Vagrantfile, we add the command to copy the built .war file (with the correct name: basic-0.0.1-SNAPSHOT.war )
   to the Tomcat8 server folder
   after the command that builds our application:

~~~shell
$ sudo cp ./build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
~~~

3.Changing front-end plugins
-

1. After some trial and error to find which plugins work for our application,
   these are the ones we are using in build.gradle:

~~~groovy
id 'org.springframework.boot' version '2.2.5.RELEASE'
id 'io.spring.dependency-management' version '1.0.11.RELEASE'
id 'java'
id 'org.siouan.frontend' version '1.4.1'
id 'war'
~~~

4.We changed the **source compatibility** from 11 to 1.8:

~~~groovy
sourceCompatibility = '1.8'
~~~

3.We also changed the frontend section, replacing the assembleScript from 'run build' to 'run webpack'

~~~groovy
frontend {
    nodeVersion = '12.13.1'
    assembleScript = 'run webpack'
}
~~~

4.Finally, we edit the scripts section of package.json to include the `"webpack": "webpack"`
key/value pair. In our case, the webpack script was already included in the scripts section of our package.json file,
so we did not have to change it:

~~~json
 "scripts": {
"watch": "webpack --watch -d --output ./target/classes/static/built/bundle.js",
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
}

~~~

4.Configure support for h2 console and enabling remote connection
-

1. We added the following lines to application.properties:

~~~
spring.datasource.url=jdbc:h2:mem:jpadb
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
~~~

**The first line indicates we are using the in-memory version of the h2 database.**
Later, this will be changed.

2. Next, we add the following line:

~~~
spring.h2.console.settings.web-allow-others=true
~~~

**This line enables the possibility of connection to a remote database.**

5.Changing the application context path
-

1. In the app.js file, we correct the path of the .war file where the GET request is to be sent:

~~~
   method: 'GET', path: '/basic-0.0.1-SNAPSHOT/api/employees'
~~~

2. **To the top of** application.properties, we add the path to the .war file:

~~~
server.servlet.context-path=/basic-0.0.1-SNAPSHOT
~~~

**Result:**

![img_1.png](img_1.png)

6.Changing reference to the CSS stylesheet
-

1. To the output be correctly formatted, in the index.html file of our app, we correct the path to the css stylesheet:

~~~ xml
<link rel="stylesheet" href="main.css" />
~~~

7.Configuring remote database connection
-
So that our application connects to the database on the **db** machine,
the following configurations must be made:

1. We don't want to use the in-memory h2 database, so we specify the path to the remote h2 database.
   We replace (or comment out) this line:

~~~
spring.datasource.url=jdbc:h2:mem:jpadb
~~~

... with this line:

~~~
spring.datasource.url=jdbc:h2:tcp://192.168.33.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
~~~

**The IP and port number should correspond to the configurations of our db VM.**

**Result**
![img_3.png](img_3.png)

2. So that the database is not dropped every time the application is executed, we add this line:

~~~
spring.jpa.hibernate.ddl-auto=update
~~~

...between these two lines:

~~~
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.h2.console.enabled=true
~~~

**Final Result:**

![img_4.png](img_4.png)

-------------------------------------------------------------

Alternative Solution - Using Vagrant and VMware
=

1.Initial Setup
-
VirtualBox is the default provider for Vagrant. However, Vagrant supports other hypervisors.
We can change Vagrant's default provider by setting the VAGRANT_DEFAULT_PROVIDER environmental variable.
To implement the alternative solution, we will be using **VMWare Workstation Player**.

Installation of the Vagrant VMware provider requires two steps.

1. First, the Vagrant VMware Utility must be installed.
   (Available at:
   https://www.vagrantup.com/vmware/downloads)

2. It is necessary to install the Vagrant VMware provider plugin, running this shell command:

~~~shell
$ vagrant plugin install vagrant-vmware-desktop
~~~

**NOTE:** It is important to ensure we are using the plugin's 1.0.0 version or higher.

We can run the shell command:

~~~shell
 $ vagrant plugin list
~~~

...and if the version is lower than 1.0.0 we can update it:

~~~shell
 $ vagrant plugin update
~~~

(The following two steps can be skipped by Windows users and first-time VMware users)  
It is also necessary to know if some outdated paths exist, performing these commands:

~~~shell
$ ls ~/.vagrant.d/gems/*/vagrant-vmware-{fusion,workstation}
~~~

If they exist, it is necessary to remove them:

~~~shell
$ rm -rf ~/.vagrant.d/gems/*/vagrant-vmware-{fusion,workstation}
~~~

**Reference:**
https://www.vagrantup.com/docs/providers/vmware

2.Configuring the VMs
-
In this section, we will adapt the class exercise to use VMware instead of VirtualBox.

1. When choosing a box, we have to take into consideration the provider we will be using.
   In our case, the box we used for the class exercise,
   **bento/ubuntu-18.04**, is supported by VMware, so we may keep this configuration.


2. We can specify the provider of our choice in the Vagranfile configurations:

**At line 3 of example 1**

~~~
db.vm.provider "vmware_desktop"
~~~

**At line 8 of example 2**

~~~
 web.vm.provider "vmware_desktop" do |v|
 ...
 ...
~~~

3. We cannot use the same network and the same IP addresses for the new machines as we used for the class assignment.

![img_5.png](img_5.png)

Network 192.168.57.0/24 is already taken by VirtualBox Host-Only Ethernet Adapter #2.
So, we change the IP addresses of the new machines in order to have a different network:

**At line 6 of example 1**

~~~
db.vm.network "private_network", ip: "192.168.58.11"
~~~

**At line 5 of example 2**

~~~
 web.vm.network "private_network", ip: "192.168.58.10"
~~~

4. We also have to change the IP address of db machine specified at application.properties to correspond to the one we defined in the Vagrantfile:

~~~
spring.datasource.url=jdbc:h2:tcp://192.168.58.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
~~~

#### Example 1 - db

~~~
#Configurations specific to the database VM
config.vm.define "db" do |db|
db.vm.provider "vmware_desktop"
db.vm.box = "bento/ubuntu-18.04"
db.vm.hostname = "db"
db.vm.network "private_network", ip: "192.168.58.11"
~~~

#### Example 2 - web

~~~
# Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "bento/ubuntu-18.04"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.58.10"

    # We set more ram memmory for this VM
    web.vm.provider "vmware_desktop" do |v|
      v.memory = 1024
    end
~~~
<br>

5. If we want to run the machines from the class assignment and alternative solution in paralell, we cannot redirect our VMs' ports to host's ports that are already occupied. Our class assignment's machines are using the host's ports 8082, 9092, and 8080. So, our alternative machines will use host ports **8083**, **9093**, and **8081**. We define this in the Vagrantfile:
~~~
db.vm.network "forwarded_port", guest: 8082, host: 8083
db.vm.network "forwarded_port", guest: 9092, host: 9093
~~~
~~~
web.vm.network "forwarded_port", guest: 8080, host: 8081
~~~

**However, if we run our application at this point, we get a 404 NOT FOUND error code from the server.**
<br>
<br>

6. By the default Spring Boot configurations, the embedded server starts on port **8080**.
Now that we changed the ports of the host where the guest machines' ports are redirected to, the application is not running on the expected port, 8080, but on port **8081**. This is the reason our application failed in the previous step. <br> <br>To solve this problem, we must add a new configuration to the top of application.properties, specifying the port of the host machine that redirects to the port of the guest machine where the application is running:
~~~
server.port=8081
~~~

**It works!**

![img_7.png](img_7.png)

-----

Conclusions
=

Using Vagrant automates the process of creating and configuring virtual machines,
since all the machine's settings can be defined in a text file - this is an example of
**Infrasctructure as Code**.
So, sharing and replicating a virtual machine becomes as easy as sharing this text file.
Regarding using different hypervisors: it is simple to instruct Vagrant
to use the provider of our choice, as we have seen above.
In the course of executing the tasks for this assignment,
I consider VirtualBox and VMware very similar, and both serve the purpose well.
However, there are some differences between offered functionalities, that may be important to take into account when working on bigger projects.
We have done some research, and here is a summarized comparison between the two:


|                                               | VirtualBox                                                                                     | VMware Workstation Player                                              |
|-----------------------------------------------|------------------------------------------------------------------------------------------------|------------------------------------------------------------------------|
| **License**                                   | Free and open-source <br>(though Extension Pack is proprietary)                                | Free for non-commercial purposes                                       |
| **Created machine detection**                 | Detects machines created using Vagrant automatically                                           | Does not detect machines automatically, they have to be added manually |
| **Hardware Virtualization**                   | Yes                                                                                            | Yes                                                                    |
| **Software Virtualization**                   | Yes                                                                                            | No                                                                     |
| **Snapshots**                                 | Yes                                                                                            | No                                                                     |             
| **Clones**                                    | Yes                                                                                            | No                                                                     |
| **Speed**                                     | Slower                                                                                         | Faster                                                                 |
| **Supprted Virtual Disk Formats**             | VDI, VMDK, VHD, HDD                                                                            | VMDK                                                                   |
| **Pre-Allocated Virtual Disks**               | Yes (called **fixed**)                                                                         | Yes (called **thick provisioned**)                                     |
| **Dinamically Virtual Disks**                 | Yes (called **allocated**)                                                                     | Yes (called **thin provisioned**)                                      |
| **Video Memory**                              | 128 MB maximum                                                                                 | 2GB maximum                                                            |
| **User Interface**                            | GUI and Command-Line                                                                           | GUI and Command-Line                                                   |
| **Virtual Network Modes**                     | Not attached, NAT, NAT Network, Bridged adapter, Internal network, Host-only adapter, UDP, VDE | NAT , Host-only network, Bridged adapter                               |
| **3D Graphics**                               | Has to be manually enabled                                                                     | Enabled by default                                                     |
| **Hypervisor Type**                           | Type 2                                                                                         | Type 2                                                                 |
| **Connecting USB devices from host to guest** | Yes, USB 1.0                                                                                   | Yes, 2 devices maximum, USB 3.0                                        |