# DEVOPS 2021-2022
## CA1 - VERSION CONTROL
##### Salomé Pereira - 1211795@isep.ipp.pt 

---------------------------------------------------------
# Version Control with Git
# Part 1

## Introduction - Initial Setup

Since Git is the VCS we use in our Switch group project, it was already setup and configured in my workstation.
In order to complete the assignment, I used the Bitbucket repository that was created in the first class, 
according to the instructions.
For the purpose of better organization and clarity, I broke down the assignment in smaller, focused tasks.
Next, in order to register these tasks, I activated my repository issue tracker.  
(Open repository> Left Bar > Settings >IssueTracker> Select private issue tracker > Save).  
These tasks were registered as issues.  
This issue-tracking system has the advantage of being possible to reference issues 
and change their status through commit messages.

## Basic Git commands
The content of the repository was manually copied onto newly created folder named CA1, except for the .git directory
and the .gitignore file. These changes were then committed to the remote repository using the following commands:

>cd  C:\projswitch\devops\devops-21-22-atb-1211795

>git status

which outputs:
>On branch master
>Your branch is up to date with 'origin/master'.
>
>Untracked files:  
>(use "git add <file>..." to include in what will be committed)  
>CA1/  
>
>nothing added to commit but untracked files present (use "git add" to track)

As we can see, the new folder was detected by Git. We want it to be tracked, so we run:
>git add .

The . adds all files to the staging area.
Now if we run the command:
>git status
> 
It outputs:
>On branch master
>Your branch is up to date with 'origin/master'.
>
>Changes to be committed:
>(use "git restore --staged <file>..." to unstage)
>new file:   CA1/.gitignore
>new file:   CA1/.mvn/wrapper/MavenWrapperDownloader.java  
>...  
> ... more files, a lot of files ...  
>...  
>   new file:   CA1/tohtml

CA1 folder and its content is now on the staging area. 
At this point we are "preparing" the commit, so we can still add and remove files from the staging area.
When we are ready we can commit:

>git commit -a -m "Create CA1 folder with necessary content resolving #1"

Output:

>[master defbf6b] Create CA1 folder with necessary content resolving #1  
>142 files changed, 10466 insertions(+)  
>create mode 100644 CA1/.gitignore  
>create mode 100644 CA1/.mvn/wrapper/MavenWrapperDownloader.java  
> 
>...more files...  
>
>create mode 100644 CA1/tohtml  

Finally, we can publish our changes in the remote repository:

>git push origin master


### Tagging

Now that the folder with the code is in the repository, we want to tag this version 

By default, a tag applies to the last commit.
However, it is possible to apply a tag to a previous commit, by specifying the commit reference.
For example:  

>git tag < tagname > < commit ref >

There are two possibilities when it comes to tags:
* Annotated
* Lightweight

In order to create a lightweight tag, we do not provide a -a, -s, or -m flag.
For an annotated tag, we can use the -a flag, and it is necessary to provide a message. 

To create our first tag, we do the following steps:

>git tag -a v1.1.0 -m "Initial version, resolves #2"

To publish the tags we created we run:

>git push origin --tags

Output:
>Enumerating objects: 1, done.  
>Counting objects: 100% (1/1), done.  
>Writing objects: 100% (1/1), 185 bytes | 185.00 KiB/s, done.  
>Total 1 (delta 0), reused 0 (delta 0), pack-reused 0  
>To https://bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git  
>* [new tag]         v1.1.0 -> v1.1.0  

Note: Unlike commit messages, tag messages do not interfere with Bitbucket issues.

## Code implementation

### Steps followed for the implementation of the new required jobYears feature:
1. Declare a new attribute onf int type in Employee class:  
`private int jobYears;`
2. Alter constructor to accept another parameter and define the  jobYears attribute upon object initialization; 
3. Alter run() method of DatabaseLoader class to include a value in the parameter jobYears of the Employee being created;
4. Include jobYears attribute as a comparison factor in the equals() method;  
5. Include jobYears attribute in the hashCode() method;  
6. Create getter and setter methods for the jobYears attribute;  
7. Add jobYears attribute to toString() method;  
8. Alter app.js file adding a new column to the table  
 `<th>Job Years</th>`
9. Adding a new cell to the table:  
    `<td>{this.props.employee.jobYears}</td>`

### Implemented Validations
Two validations methods were implemented:
1. For String type fields, the validation method receives the String and throws an exception if input is null or blank. 
Then, blankspace characters are trimmed from the beginning and end of the String. 
Finally, the String is returned.
2. For int type fields, the validation method throws an exception if integer value is less than zero. 
Else, it returns the number.

These validations are called in Employee constructor upon each attribute initialization.

### Testing

I used parametrized tests to ensure that, for null, empty or out of range values 
passed in Employee constructor, exceptions are thrown.
Tests also validate that for correct values, the creation of objects is successful: does not throw an exception
and created object is not null.


## Tagging, part 2

The following sequence of steps was taken to apply tag v1.2.0 ( similar to applying tag v1.1.0 followed a similar sequence):

>git tag -a v1.2.0 -m "New tested feature: jobYears"

>git push --tags

Output:
>Enumerating objects: 1, done.  
>Counting objects: 100% (1/1), done.  
>Writing objects: 100% (1/1), 184 bytes | 184.00 KiB/s, done.  
>Total 1 (delta 0), reused 0 (delta 0), pack-reused 0  
>To https://bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git    
>* [new tag]         v1.2.0 -> v1.2.0

The tag is an object.
Now, we can view existing tags:
>git tag

Output
>v1.1.0  
>v1.2.0

For the last tag (ca1-part1), we will create a lightweight tag, further analyze the difference between both types.  
The commit we want to tag is: eead9f5  
We run the command as followed:

>git tag ca1-part1 eead9f5

Now the command git tag outputs:

>ca1-part1  
>v1.1.0  
>v1.2.0  

To push the new tag:
>git push origin ca1-part1

The output is:
>Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
>To https://bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git
>* [new tag]         ca1-part1 -> ca1-part1

The tag is not an object.
If we run:
>git show ca1-part1

The output is:
>commit eead9f5eb7d224c9c835fd7b55f269613b2a1d37 (tag: v1.2.0, tag: ca1-part1)  
>Author: Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>Date:   Thu Mar 17 18:04:02 2022 +0000  
>
>Test validations for Employee fields, closing #3 and #4  


Now, let's look at an annotated tag pointing to the same commit:

>git show v1.2.0

Output:
>tag v1.2.0  
>Tagger: Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>Date:   Thu Mar 17 18:18:32 2022 +0000  
>
>New tested feature: jobYears
>
>commit eead9f5eb7d224c9c835fd7b55f269613b2a1d37 (tag: v1.2.0, tag: ca1-part1)  
>Author: Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>Date:   Thu Mar 17 18:04:02 2022 +0000  
>
>   Test validations for Employee fields, closing #3 and #4

A lightweight tag is a pointer to a certain commit.  
An annotated tag, however, is an object. As so, an annotated tag stores some information that a lightweight tag doesn't:
* Name and email of the tagger
* Date the tag was created
* Tag message



_________________________________
# Part 2 - Using Branches

## Setup
New issues were created, corresponding to the tasks of the assignment's part 2.

## Branching
The first step is to create a new branch to continue the assignment.
We will run the command:
>git checkout -b email-field

which will create the email-field branch and move the HEAD pointer to it at once. Output:
>Switched to a new branch 'email-field'

Now we want to push the branch to the remote repository

>git push origin email-field

Output:
>Total 0 (delta 0), reused 0 (delta 0), pack-reused 0  
remote:  
remote: Create pull request for email-field:  
remote:   https://bitbucket.org/salome_pereira/devops-21-22-atb-1211795/pull-requests/new?source=email-field&t=1  
remote:  
To https://bitbucket.org/salome_pereira/devops-21-22-atb-1211795.git  
> * [new branch]      email-field -> email-field  


### Code Implementation- New Feature
Let's do some work on the new branch:
1. Add new email attribute to employee.  
The sequence for pushing alterations to the remote repository is:
>git add .  
>  git commit -a -m "commit message here"
> git push origin email-field
> 
We can use the command
> git push --set-upstream origin email-field

Or
> git push -u origin email-field

To establish a link between our local branch and the remote branch.
If we do so, in subsequent commits, we only have to run:

>git push


2. Add backend support for the new email field:
   1. Generating getters and setters for email field;
   2. Adding attribute in constructor;
   3. Including attribute in equals() and hashcode();
   4. Including attribute in toString();
   5. Filling attribute in EmployeeTest and DatabaseLoader constructor calls.

3. Add frontend support for the new email field:
   1. Alter app.js file adding a new column to the table  
         `<th>Email</th>`
   2. Adding a new cell to the table:  
      `<td>{this.props.employee.email}</td>`

Let's run the app to verify everything is functional...  
It works!  
We can now make another commit, following the aforementioned steps.

###Code Implementation- New Feature Testing
1. We will subject the attribute to the same validation as other String type attributes 
and generate similar unit tests, to ensure null and empty values are not accepted.

We make a new commit.  

**Now, we are ready  to merge this branch with master following these steps:**

Returning to master branch:
> git checkout master

Output:
> error: Your local changes to the following files would be overwritten by checkout:  
CA1/readme.md  
Please commit your changes or stash them before you switch branches.  
Aborting

Something went wrong: we cannot switch branch if we have uncommitted changes.
We try again after making a commit. Output:

>Switched to branch 'master'  
Your branch is up to date with 'origin/master'.

Then:

>git merge email-field
> 
Output:

>Updating 2d8847f..46655a6  
Fast-forward  
.../com/greglturnquist/payroll/DatabaseLoader.java |   2 +-  
.../java/com/greglturnquist/payroll/Employee.java  |  25 +++-  
CA1/basic/src/main/js/app.js                       |   2 +  
.../com/greglturnquist/payroll/EmployeeTest.java   |  25 ++--  
CA1/readme.md                                      | 140 ++++++++++++++++++++-  
5 files changed, 176 insertions(+), 18 deletions(-)  

Fast-forward means the commit master pointed to is the "parent" of the commits of branch email-field.
In order to merge, the pointer only has to be moved forward.

Next, it is necessary to push:
 >git push origin master

Now that email-field branch is no longer necessary, we can delete it:

> git branch -d email-field

Output:
>Deleted branch email-field (was 46655a6).

The existence of this branch is, however, permanently recorded.

##Tagging
**Now, we tag this version as v1.3.0.**  

We will create an annotated tag:
> git tag -a v1.3.0 -m "New email field"

> git push origin v1.3.0


After this, the commit history in Bitbucket appears linear, even though 
we created branches. That is due to master branch having not been developed in parallel.

##Branching - Second Branch
###Code Implementation- Bug fixing

For the second task of part 2, we want to improve our validations. 
At the moment the email field allows the insertion of any string 
as long as it is not null or empty. 
We want to prevent insertion of strings that are not effectively emails. Let's fix this!
First, we create a new branch:
> git  checkout -b fix-invalid-email

> git push -u origin fix-invalid-email

We are now on branch fix-invalid-email, we have pushed it to remote, 
and we have set an upstream connection between our local branch and the remote.

1. Since email field has extra business rules that don't apply to other String attributes,
we have to be sure to enforce them. For that, we will create a new method, `validateEmail(String email)` that uses the regular expression that follows:  
`"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$"`  
Then we use a Matcher to verify if the input string matches the regex.


2. As for the unit tests, we add some invalid emails to the value source of 
`ensureCreationFailsWithInvalidValuesEmail(String input)` test.


(Note: this is the same solution group 6 is using in the group project.)  

Two commits were made. The bux is fixed!  
Now we follow these steps to merge fix-invalid-email into master


> git checkout master

>git merge email-field

>git push origin master

Followed by deleting the branch we no longer need:

> git branch -d fix-invalid-email

Applying an annotated tag:

> git tag -a v1.3.1 -m "Fix invalid email"

> git push origin v1.3.1

We have finished the second part of the assignment. Now we create a lightweight tag to mark finished part 2:

>git tag ca1-part2

>git push origin ca1-part2

-----------------------------------------------
# Alternative - Version control with Mercurial
# Part 1

## Introduction - Initial Setup

I chose to implement the alternative solution alongside the requirements for CA1.
The chosen VCS alternative to git was Mercurial
Since Bitbucket works exclusively with Git, it was necessary look for an alternative hosting platform 
that supports Mercurial to create a new repository.
The alternative I chose was SourceForge, for being a platform dedicated to providing resources focused on the Open Source community. 
Hosting open-source software projects in SourceForge is free of charge.


The repository is available on https://sourceforge.net/p/devops-21-22-atb-1211795

This was the command used to initialize mercurial repository, after changing to the directory that will become our working directory:

    hg clone ssh://pereira1211795@hg.code.sf.net/p/devops-21-22-atb-1211795/code devops-21-22-atb-1211795-code

SourceForge makes available a ticket managing system:
Tickets can be registered and managed manually.  
I experimented with referencing tickets and changing their status through commit messages, however, it did not work. 
I did not find information about how to do it, and I assume this functionality does not exist.


## Creating a .hgignore file

Like Git, it is possible to instruct Mercurial to ignore files whose names match certain patters.
This is done through the .hgignore file, where we can write these patterns. 
Our first task will be to create it and to commit it to the repository.
Let's create it, changing to the working directory and running the following command:

>echo > .hgignore

The first line of the .hgignore file declare the syntax used for the patterns.
There are two possibilities:  
- syntax: glob  
- syntax: regexp

I chose the first alternative, as it is simpler, and is very similar to the syntax of the .gitignore files.
We can transpose the content from .gitignore to .hgignore

Then, the command
>hg status

Produces as output:
>? .hgignore

The ? that precedes .hgignore informs us the file is not being tracked.
Here is the meaning of the preceding symbols, from the manual:

* M = modified  
* A = added  
* R = removed  
* C = clean  (status command can be used to compare differences between revisions, this means the file was unchanged)  
* ! = missing (deleted by non-hg command, but still tracked)  
* ? = not tracked  
* I = ignored
* = origin of the previous file (with --copies)  

So, we run the following commands:

>hg add 
> 
Output:
>adding .hgignore
> 
Followed by:
>hg commit -m "Initial commit. Add .hgignore file"

>hg push

Output: 
>pushing to ssh://pereira1211795@hg.code.sf.net/p/devops-21-22-atb-1211795/code  
>remote: Keyboard-interactive authentication prompts from server:  
>remote: End of keyboard-interactive prompts from server  
>searching for changes  
>remote: adding changesets  
>remote: adding manifests  
>remote: adding file changes  
>remote: added 1 changesets with 1 changes to 1 files  
>remote: <Repository /hg/p/devops-21-22-atb-1211795/code> refresh queued.  


## Mercurial tags
The next step is to add the application code. Now that we have the .hgignore file,
we can do it. We will use the sequence of steps mentioned above:

>hg add 

> hg commit -m "Add application code resolving #2"

> hg push

After adding the application code to our repository, we went to tag it as v1.1.0

There are two types of mercurial tags: "regular" and local. Local tags remain in the local repository 
and cannot be pushed to remote, they can be created by adding the -l option to the command. 
So, for this assignment, we will only use regular tags.
Before applying any tag, lets run the hg tags command:
>hg tags

Which outputs:
>tip                                1:1630965fafe2


A changeset is the equivalent of a Git commit. Each changeset
is identified by a 40 digit hexadecimal number. 
In local repositories, changesets have a revision number, which is sequential, 
and appears before the changeset ID separated by a colon. 
However, these numbers are not shared identifiers of changesets, they are valid within that one repository.
In other clones of the repository, the same changeset may have a different revision number.  

**Tip is a special tag, that identifies the most recent changeset in our local repository.**

We want to tag revision one, we run:

>hg tag -r 1 v1.1.0

When we run:
>hg log

The output shows us a new changeset, the addition of tag v1.1.0:

>changeset:   2:46e610972ab4  
>tag:         tip  
>user:        Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>date:        Wed Mar 16 15:02:17 2022 +0000  
>summary:     Added tag v1.1.0 for changeset 1630965fafe2  
>
>changeset:   1:1630965fafe2  
>tag:         v1.1.0  
>user:        Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>date:        Wed Mar 16 14:05:47 2022 +0000  
>summary:     Add application code resolving #2  
>
>changeset:   0:5012fb45580f
>user:        Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>date:        Wed Mar 16 13:41:25 2022 +0000  
>summary:     Initial commit. Add .hgignore file  


After that:
>hg push

The tag was added successfully to the repository.
Unlike in Git, creating a tag and removing a tag constitute changesets, a commit is generated.
If we do not provide a message, Mercurial provides a message for us.
We could have added a commit message in addition to the tag to change the automatically generated summary for changeset 2 
(Added tag v1.1.0 for changeset 1630965fafe2).


### Applying the second tag:

After we have followed the same steps mentioned in 
the previous section dedicated to Git to implement the jobYears field, 
the task is concluded, and we want to tag the revision:

>hg tag v1.2.0 -m "New tested feature: jobYears"

>hg commit -m "Apply tag to implementation of new feature"

(Note: this commit message is incorrect: some other files were changed, unrelated to tags, 
thus the necessity of creating another changeset. 
I did not realize it at the time. 
hg ci is unnecessary for applying a tag.)

>hg push

>pushing to ssh://pereira1211795@hg.code.sf.net/p/devops-21-22-atb-1211795/code  
>remote: Keyboard-interactive authentication prompts from server:  
>remote: End of keyboard-interactive prompts from server  
>searching for changes  
>remote: adding changesets  
>remote: adding manifests  
>remote: adding file changes  
>remote: added 2 changesets with 4 changes to 4 files  
>remote: <Repository /hg/p/devops-21-22-atb-1211795/code> refresh queued.  

>hg tags

>tip                                6:d78843587c3f  
>v1.2.0                             4:e66d5d8cfd20  
>v1.1.0                             1:1630965fafe2  
  
Now we can refer to a changeset by its tag:
>hg log -r v1.2.0

changeset:   4:e66d5d8cfd20
tag:         v1.2.0
user:        Salome Pereira [1211795] <1211795@isep.ipp.pt>
date:        Thu Mar 17 18:43:38 2022 +0000
summary:     Test validations for Employee fields



### Applying third mercurial tag

Now that the first part of the assignment is done, we want to apply tag ca1-part1.
Let's see the tags we currently have:

> hg tags

tip                                6:d78843587c3f
v1.2.0                             4:e66d5d8cfd20
v1.1.0                             1:1630965fafe2

I ran:
>hg tag ca1-part1 - r e66d5d8cfd20 -m "Finished first part of the assignment"  

Which contained a mistake( - r instead of -r). The result:

>hg tags

>tip                                7:bfb41af48443  
>r                                  6:d78843587c3f  
>e66d5d8cfd20                       6:d78843587c3f  
>ca1-part1                          6:d78843587c3f  
>\-                                  6:d78843587c3f  
>v1.2.0                             4:e66d5d8cfd20  
>v1.1.0                             1:1630965fafe2  

Four tags were created, and added to the previous changeset d78843587c3f, not e66d5d8cfd20 as we intended.
Let's correct this mistake:

> hg tag --remove ca1-part1 - r e66d5d8cfd20

Let's see if the wrong tags are gone:
> hg tags

They no longer appear: 
>tip                                8:1c62de2c7879
>v1.2.0                             4:e66d5d8cfd20
>v1.1.0                             1:1630965fafe2

However, a history of the tags that existed and the changesets they pointed to is kept in the file .hgtags.  

Now we Apply the tag correctly:

> hg tag ca1-part1 -r e66d5d8cfd20 -m "Finished first part of the assignment"

> hg tags
> 
tip                                9:07ca426e7074
v1.2.0                             4:e66d5d8cfd20
ca1-part1                          4:e66d5d8cfd20
v1.1.0                             1:1630965fafe2

> hg commit -m "Apply third tag"

Output:
>nothing changed (2 missing files, see 'hg status')

(As we have seen before, if no other changes were made, committing was an unnecessary step)

>hg push

>pushing to ssh://pereira1211795@hg.code.sf.net/p/devops-21-22-atb-1211795/code  
>remote: Keyboard-interactive authentication prompts from server:  
>remote: End of keyboard-interactive prompts from server  
>searching for changes  
>remote: adding changesets  
>remote: adding manifests  
>remote: adding file changes  
>remote: added 3 changesets with 3 changes to 1 files  
>remote: <Repository /hg/p/devops-21-22-atb-1211795/code> refresh queued.  
>

Three chagesets were pushed: creating the wrong tags, removing the wrong tags, and creating the right tag.

_______________________________________________
# Part 2

## Setup
New tickets were created, corresponding to the tasks of the assignment's part 2.

## Branching with Mercurial
When we run 
>hg branches

The output informs us there is one branch, default (equivalent to main or master in Git):
>default                        9:07ca426e7074

We will now create a new branch, using the following command:
>hg branch email-field

Output:
>marked working directory as branch email-field  
>(branches are permanent and global, did you want a bookmark?)

Now let's see the branch we are currently on:
>hg branch

Output:
>email-field

Mercurial switched automatically to the new branch.  
However, if we run 
>hg branches

Output:
>default                        9:07ca426e7074

We don't see the new branch yet. First we have to commit it (ci is the same as commit):

>hg ci -m "Start new email-field branch"

How if we run hg branches again:

>email-field                   10:12455988a8b0
>default                        9:07ca426e7074 (inactive)

We can switch back to default with the command update:
>hg update default

And to email- back again:

>hg update email-field

Finally, let's create this new branch in the remote repository. 

>hg push --new-branch


**Pushing to remote branches**

Unlike git, running hg push will push all branches. To push only one branch we run:
>hg push -r branchName


## Code Implementation- New Feature
For code implementation, the same steps of the Git section were followed. the sequence for committing to remote is
> hg ci -m "commit message here"  

> hg push

After replicating the same steps,
we now merge the branches by running the following command sequence:

>hg branches 

Output:
>email-field                   14:283f170fd82a  
>default                        9:07ca426e7074 (inactive)  

The output tells us we are on branch email-field.  
Let's change back to default:
> hg update default

Next, I tried:
> hg merge email-field

In the meanwhile, I had to confirm the removal of some files, for which I used TortoiseHg, and correct the .hgignore file
After committing, we can try again. The output:
>9 files updated, 0 files merged, 0 files removed, 0 files unresolved  
(branch merge, don't forget to commit)

Then:
>hg ci -m "Merge email-field branch into default"

Next, we apply the tag (-r 19 refers to the 19th changeset, the version we want to tag):
>hg tag -r 19 v1.3.0 -m "New email field"

Followed by:
>hg push

At last, our work is done in email-field branch and we no longer need it. 
So, we will mark it as closed. First, we switch to email-field branch:
>hg update email-field

To close it, we run the following commands:
> hg ci --close-branch -m "Email feature is done"

Next, we switch back to default:
>hg update default

Now if we run:
>hg branches 
> 
The output is:
>default                       20:c57ae0247f20

The email-field branch no longer appears. However, if we run:
>hg branches -c
> 
The output shows us that email-field is closed:
>default                       20:c57ae0247f20  
>email-field                   21:36a44c0c1c26 (closed)


At last, our changes must be pushed to remote.

However, at this point the commit history looks like this:

![Commit history of first branch](tree.png "Commit history of first branch")

Maybe it would have been best to close the branch first and then merge it with default.

## Second Branch
For the email validations, we will follow a slightly different sequence of commands:

Starting the new branch:

>hg branch fix-invalid-email

> hg ci -m "Start new fix-invalid-email branch"

>hg push --new-branch

We make our changes in the project, as demonstrated above in the section dedicated to version control with git.
We commit each changeset with the following commands:

> hg ci -m "commit message here"  

> hg push

We made two commits.
Finally, when our work is done in this branch we close it:

> hg ci --close-branch -m "Email-specific validations are implemented"

Then we switch to default branch and merge fix-invalid-email branch into default

> hg update default

> hg merge fix-invalid-email

>hg ci -m "Merge fix-invalid-email branch into default"

Using hg log, we confirm the number of the changeset we want to tag:

>changeset:   26:894cab437a09  
>tag:         tip  
>parent:      20:c57ae0247f20  
>parent:      25:206f03565579  
>user:        Salome Pereira [1211795] <1211795@isep.ipp.pt>  
>date:        Sat Mar 26 15:25:20 2022 +0000  
>summary:     Merge fix-invalid-email branch into default  

It's the tip, revision 26.  
At last, we apply the tags:

>hg tag -r 26 v1.3.1 -m "Implement email-specific validations"

>hg tag -r 26 ca1-part2 -m "Finished part 2 of the class assignment"

To conclude:

>hg push

This time, we did the sequence (almost) right 
(I applied the ca1-part2 tag before v1.3.1 as is evident in the commit history).

![Commit history of second branch](tree2.png "Commit history of second branch")
______________________________________________________________________________________________

# Conclusion
For the most part, Git ang Mercurial commands are very similar.
The main difference I noticed in the context of this assignment is between the functionalities 
and options offered in terms of tags and branching.

I noticed operations with Mercurial and SourceForge are slower.
SourceForge takes a while to refresh the commit tree page after a commit is made.

Operations like merges and applying and deleting tags, closing branches
constitute their own changesets and as so, are registered in the repository.
This level of detail may or may not be interesting for developers and teams

I quite enjoy working with Bitbucket issues, since it is possible to relate them to commits.
It is a very helpful tool that favours organization, focus and good record-keeping.  
In my view, SourceForge tickets are not as interesting as Bitbucket issues. They have to be managed manually,
which feels like a small extra task, that can be easily forgotten.

SourceForge represents branches in a visually evident way, the same does not always happen in Bitbucket.

