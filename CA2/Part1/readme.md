# DEVOPS 2021-2022
## CA2 PART1- BUILD TOOLS
##### Salomé Pereira - 1211795@isep.ipp.pt 

---------------------------------------------------------
# Build Tools with Gradle

## 1. Add a new task to execute the server

To add a new task, we have to edit the file build.gradle
We can see we already have a task: runClient. It is a subtype of JavaExec and depends on task classes.
Classes is a task that assembles the main classes of the application.
This task has two attributes inherited from JavaExec:

* classpath 
* mainclass

1. We will create a task of type JavaExec, that dependsOn classes, we declare the type and 
dependency inside the parenthesis:  
`task runServer(type:JavaExec, dependsOn: classes)`

2. We will mantain the same group as runClient: DevOps.  
   
3. We give it a meaningful description, such as "Launches a chat server on localhost:59001"
4. We will copy the classpath attribute from runClient task, our class is located in the same directory.
5. As for the mainclass attribute, it is the name of the package that contains the class we want to run, followed by the class name. We will change it to:  
`basic_demo.ChatServerApp`
6. "args" should be followed by each of the arguments to be contained in the array of Strings (`String[] args`) that is passed to the main method of the class when we run it.  
If we analise the ChatServerApp, it needs one argument to launch, the serverport. The chatServer should run on port 59001 That's the argument we will pass:  
`args '59001'`

If we run `gradle runServer`, it works!  

**We can now launch the Clients!**

## 2. Add unit test
1. We will create the root of directories where the test classes should be.
for example, running the command 
`mkdir -p test/java/basic_demo` after changing our working directory to src folder of the project we are working on.

    1. On IntelliJ Idea, we generate a test class. we select JUnit4 as our library.
    2. Our build.gradle has changed! on `dependencies {}` we now see two lines:
            `implementation 'junit:junit:4.13.1'`
    JUnit 4 is necessary for implementing unit tests.
    Let's change it to our preferred version `'junit:junit:4.12'`

    Let's see if we can run the tests, by running on the command line:  
    `gradle test`

> Task :compileJava UP-TO-DATE  
 >Task :processResources UP-TO-DATE  
 >Task :classes UP-TO-DATE  
 >Task :compileTestJava  
 >Task :processTestResources NO-SOURCE  
 >Task :testClasses  
 >Task :test  
>
>
>BUILD SUCCESSFUL in 1s  
>4 actionable tasks: 2 executed, 2 up-to-date  

**Does this mean it worked? How can we be sure?
Let's setup our test so that it fails, run the task again and see what happens:**

> Task :compileJava UP-TO-DATE  
> Task :processResources UP-TO-DATE  
> Task :classes UP-TO-DATE  
> Task :compileTestJava UP-TO-DATE  
> Task :processTestResources NO-SOURCE  
> Task :testClasses UP-TO-DATE  
>  
> Task :test FAILED  
>  
>basic_demo.AppTest > testAppHasAGreeting FAILED  
>java.lang.AssertionError at AppTest.java:14  
>  
>1 test completed, 1 failed  
>  
>FAILURE: Build failed with an exception.  
>  
>* What went wrong:  
>  Execution failed for task ':test'.  
>There were failing tests. See the report at: file:///C:/projswitch/devops/devops-21-22-atb-1211795/CA2/Part1/luisnogueira-gradle_basic_demo-8b7abb1a82e8/build/reports/tests/test/index.html
>
>* Try:  
> Run with --stacktrace option to get the stack trace.  
> Run with --info or --debug option to get more log output.  
> Run with --scan to get full insights.  
>  
>* Get more help at https://help.gradle.org  
>  
>BUILD FAILED in 2s  
>4 actionable tasks: 1 executed, 3 up-to-date  

**Yes, it works!**

## 3. Add a new task of type Copy 
1. To create a task called copy, of type Copy, we declare its type inside the parenthesis:  
`task copy(type: Copy)`

2. We add a group and description:   
`group = "DevOps"`  
`description = "Copies the content of src directory to backup folder"`
3. We declare the folder that we want to copy:  
`from 'src'`
4. We declare the destination folder:  
`into 'backup'`

5. Now, let's try it!   
`gradle copy`  

**If we check the directory of our project, there it is, the backup folder with a copy of the contents of the src folder!** 


## Add a new task of type Zip 

1.To create a task called zip, of type Zip, we declare its type inside the parenthesis:
`task zip(type: Zip)`

2. We define the group and the description.
3. We define the folder that we want to compress into the archive:  
`from "src"`
4. We define the destination folder where our archive will be generated. 
This can be a relative path or an absolute path:  
`destinationDirectory = 
file("zip")`  
4.We have to give a name to the archive we want to generate:  
`archiveFileName = "src.zip"`
5. Let's now run the task:
`gradle zip`  

**The task created a folder called zip that contains an achive called src.zip, with the content of the src folder.**



## Mark repository with the tag ca2-part1

To apply the tag, we follow the steps we already know from the previous assignment:

1. `git tag ca2-part1`
2. `git push origin ca2-part1`





