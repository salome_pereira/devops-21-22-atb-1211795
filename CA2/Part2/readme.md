DEVOPS 2021-2022
=
CA2 PART2- BUILD TOOLS
----------------

##### Salomé Pereira - 1211795@isep.ipp.pt

---------------------------------------------------------
Build Tools with Gradle
=

1.Setup
--

### 1.To initiate our work, we create a branch and checkout with the command:

~~~shell
$ git checkout -b tut-basic-gradle
~~~

Then, we create a folder to save our work and push our branch to the repository, setting an upstream connection:

~~~~ shell
$ cd CA2/Part2

$ mkdir tut-basic-gradle

$ git commit -a -m "Commit message here"

$ git push -u origin tut-basic-gradle
~~~~

### 2.Generating a Gradle Spring Boot Project

The project can be generated with preset configurations according tou our need at this website:   
**https://start.spring.io/**

![img.png](img.png)

Definitions were copied from the pom.xml file from the basic folder of the project we are basing our work into, and from
the ca2-part2 assignment sheet. We are using the lates Gradle stable version, as recommended. We choose the Java version
that corresponds to the one we are using in our machine:

~~~shell
 $ java --version 
  
 java 11.0.12 2021-07-20 LTS  
 Java(TM) SE Runtime Environment 18.9 (build 11.0.12+8-LTS-237)  
 Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.12+8-LTS-237, mixed mode)  
~~~

Finally, we download the project as a Zip file and extract it into our assignment's directory.

### 3.Removing and adding folders and files

We open the terminal in our repository root directory and run:

~~~~shell
 $ rm -R CA2/Part2/tut-basic-gradle/react-and-spring-data-rest-basic/src
 
 $ cp -R CA1/basic/src  CA2/Part2/tut-basic-gradle/react-and-spring-data-rest-basic
 
 $ cp CA1/basic/webpack.config.js CA2/Part2/tut-basic-gradle/react-and-spring-data-rest-basic
 
 $ cp CA1/basic/package.json CA2/Part2/tut-basic-gradle/react-and-spring-data-rest-basic
 
 $ rm -R CA2/Part2/tut-basic-gradle/react-and-spring-data-rest-basic/src/main/resources/static/built/
~~~~

2.Viewing tasks:
--

~~~shell
$ ./gradlew tasks
~~~

There are 28 pre-configured tasks available, grouped as followed:

- Application tasks
- Build tasks
- Build Setup tasks
- Documentation tasks
- Help tasks
- Verification tasks

3.Running Spring Boot:
--

One of these tasks is **bootRun**, under Application tasks group. Let's try it:

~~~shell
$ ./gradlew bootRun
~~~

As expected, when loading localhost:8080, we are presented a blank page, which leads us to the next topic of our
assignment.

4.Configuring front-end support
--

So we can have a front-end, we have to configure the org.siouan.frontend plugin:

1. First, we add `id "org.siouan.frontend-jdk11" version "6.0.0"`, as we have seen previously, we are using java 11. The
   plugin section now looks as this:

~~~groovy
   plugins {
    id 'org.springframework.boot' version '2.6.6'
    id 'io.spring.dependency-management' version '1.0.11.RELEASE'
    id 'java'
    id "org.siouan.frontend-jdk11" version "6.0.0"
}  
~~~

2. However, this is not enough to get our front-end up and running. When we run the app, our page is still blank. Let's
   add this block of code to build.gradle:

~~~groovy
   frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run build"
    cleanScript = "run clean"
    checkScript = "run check"
}  
~~~

3. Something else is needed. Let's update the package.json file, it should be like this:

~~~json 

"scripts": {  
      "watch": "webpack --watch -d --output ./target/classes/static/built/bundle.js",
      "webpack": "webpack",  
      "build": "npm run webpack",  
      "check": "echo Checking frontend",  
      "clean": "echo Cleaning frontend",  
      "lint": "echo Linting frontend",  
      "test": "echo Testing frontend"  
},  
~~~

Now, if we run

~~~shell
 $ ./gradlew tasks
~~~

We can see we have two new groups...

- Frontend tasks
- Publishing tasks

...and ten new tasks!  
**But our front-end still doesn't display the employees...**

4. Of course! We have to build our project first.  
   Our static folder only has file main.css.  
   After running:

~~~ shell
$ ./gradlew build
~~~

...we have an extra folder, named **built**. Let's try to run the app again:

~~~shell
$ /.gradlew bootRun
~~~

**Result:**

![img_4.png](img_4.png)

**It works!**

5.First task: Copying Jars
--

We don't know much about jar. Let's investigate:

There are two tasks related to generating jar:

- **jar** : Assembles a jar archive containing the main classes. (
  react-and-spring-data-rest-basic-0.0.1-SNAPSHOT-plain.jar)


- **bootJar** : Assembles an executable jar archive containing the main classes and their dependencies. This jar can be
  used to boot our application.(react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar)

When we run the build task, both these tasks are run, so we have both jars. They are saved in **build/libs** directory
located at the root folder of our project.

1. We will call our task **copyJars**;
2. It's type should be **Copy**;
3. In order to work, this task **depends on** task **build**;
4. This task's group will be **DevOps**;
5. We will add a brief sentence explaining what it does as a description;
6. The task will copy both jars from the **build/libs** into **dist** folder, also located at the root of our project.  
   We will use relative paths. (This has the advantage of working for any machine we clone our project into).

**Result:**

~~~groovy
    task copyJars(type: Copy, dependsOn: build) {  
    
       group = "DevOps"  
       description = "Copies jar viles into dist folder at the project root"  
      
        from 'build/libs'  
        into 'dist'  
    }  
~~~

6.Second Task: Cleaning Webpack Generated Files
--

1. When we run the build task, the webpack tool generates a folder, **built**, in **src/main/resources/static**. We want
   to create a task to remove it.
   <br><br>

   1. Our task will be called **cleanWebpack**;
   2. It's type should be **Delete**;
   3. The task's group will be **DevOps**;
   4. We will add an explanation of what it does as a description;
   5. We specify the path of the folder we want to delete.

**Result:**

~~~groovy
task cleanWebpack(type: Delete) {

    group = "DevOps"
    description = "Deletes files generated by the webpack tool."

delete 'src/main/resources/static/built'
}
~~~

2. Now, we also want to customize default **clean** task, so it deletes the build directory, as well as the files
   generated by webpack.
   <br><br>
   1. We will change the description of the **clean** task to reflect our changes
   2. We declare the dependency on task **cleanWebpack**
   3. We add an action, printing a message that informs the user that the webpack generated files will be cleaned as
      well.

**Result:**

~~~groovy
 clean {
    description = " Deletes the build directory and files generated by webpack tool."
    dependsOn(cleanWebpack)

    doFirst {
        println 'Files generated by webpack tool will also be cleaned.'
    }
}
~~~

Now, everytime we run the **clean** task, **cleanWebpack** is run as well. We are ready to commit, merge, and push!
______________________________________________________________________________________

Build Tools with Maven
==

1.Introduction, Setup
------
The alternative build tool we will analyse is Maven. This is the tool we have been using to build our group project, so,
we will take this opportunity to get more familiar with its workings.

We will develop our work on a new branch, **tut-basic-maven**.

2.POM
--
POM stands for **Project Object Model**. It is Maven's equivalent of **build.gradle** file. It uses xml language, so it
is structured by nested tags. The pom.xml file is essential, as it contains information about the project and the
necessary configurations for the build, such as dependencies. pom.xml files may contain a lot of project details,
necessarily embraced by the `<project> </project>` tag set, the root, but the following information is mandatory:

- **modelVersion**;


- **groupId**;


- **artifactId**;


- **version**.

3.Lifecycles and Phases
--

In Maven, the build process is clearly and specifically defined- a **lifecycle**. There are three built-in life cycles:

- **default**: the main life cycle,compiles, test, packages, installs and deploys the project;


- **clean**: removes all files generated by the build, cleaning our workspace;


- **site**: generates the project's site documentation.

Each lifecycle is broken down in **phases**. The phases are sequential. It is possible to execute a lifecycle partially,
up to a certain phase. This mean all the phases that precede the one we are executing will be executed as well,
according to the sequence. In order to execute, a build phase must have **plugin goals** bound to it.

**To execute a phase:**
~~~shell
$ mvn some-phase
~~~



4.Plugins
--

Plugins help us extend Maven functionalities: each plugin provides a set of goals and these goals may be added to phases.
Plugins can be added to our project, in the pom.xml plugins section, and configured.
There are many Maven plugins available in online repositories, for instance Maven Central,
it is also possible to develop custom Maven plugins.


5.Goals
--

A plugin goal is a task that acts on the
project. A goal may or may not be bound to phases. When a goal is bound to phases, it is executed when the phases are
executed. If a goal is not bound to a build phase, it can be executed by direct invocation from the command line,
independently of lifecycles or phases.

**To execute a goal:**
~~~shell
$ mvn some-plugin:some-goal
~~~



6.Viewing information about plugins and goals:
--
This command output enumerates all the plugins our project uses, and is very extensive.

~~~shell
$ mvn help:effective-pom
~~~

The following command informs us of the goals that exist for a certain plugin:

~~~shell
$ mvn help:describe -Dplugin=plugin-name-here
~~~

7.Second Task: Cleaning Webpack Generated Files
--

To meet this requirement we will use the **Maven Clean Plugin**. Its configuration is very simple:

1. We add the plugin to the section defined by `<build>
   <plugins>
   </plugins></build>` tags of our pom.xml

~~~xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-clean-plugin</artifactId>
    <version>3.2.0</version>
</plugin>
~~~

2. To our plugin section,(inside `<plugin> </plugin>`)  
   we add the set of tags that will enable us to specify the additional files to be cleaned:
   `<configuration><filesets>  </filesets></configuration>`


3. Inside the previous set of tags, we add another set of tags,
   `<fileset> </fileset>`.


4. Inside the previous set of tags, we add the directory we want to delete:
   Optionally, we can use:
~~~xml
<includes>
    <include>some-file.doc</include>
</includes>
~~~
or:
~~~xml
<excludes>
    <exclude>some-file.doc</exclude>
</excludes>
~~~
if we need to be more specific about the files in the directory to be eliminated or kept.

**Result:**

~~~xml

<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-clean-plugin</artifactId>
    <version>3.2.0</version>
    <configuration>
        <filesets>
            <fileset>
                <directory>src/main/resources/static/built</directory>
            </fileset>
        </filesets>
    </configuration>
</plugin>
~~~

**Now, everytime clean is run, the files generated by webpack will be cleaned as well.**

8.First Task: Copying Jar
--
To meet this requirement, we will use the **Maven Resources Plugin** and we will configure one of its goals, the **copy-resources** goal.
1. We add the plugin to the plugins section of our pom.xml file;  

2. The plugin will have one execution of one of its goals.  
   (One `<execution> </execution>` tag set inside `<executions> </executions>`);

3. We define an id for the execution;

4. We bind the execution to the **package** phase;

5. We specify the goal: **copy-resources**;

6. We specify the path to the output directory: **dist**;

7. Inside the ` <resources>  </resources>` tag set, we specify the files and directories that we want to copy:    
   1. We create a new set of tags : `<resource> </resource>`;  
   2. We indicate the path to the directory where the files to be copied are located;  
   3. We specify which files we want to include or exclude.  

**Result:**

~~~xml

<plugin>

    <artifactId>maven-resources-plugin</artifactId>
    <version>3.0.2</version>
    <executions>
        <execution>
            <id>copy-jar</id>
            <phase>package</phase>
            <goals>
                <goal>copy-resources</goal>
            </goals>
            <configuration>
                <outputDirectory>dist</outputDirectory>
                <resources>
                    <resource>
                        <directory>target</directory>
                        <includes>
                            <include>*.jar</include>
                        </includes>
                    </resource>
                </resources>
            </configuration>
        </execution>
    </executions>
</plugin>

~~~

**This task is bound to the package phase.** To execute it, we have to run:
~~~shell
$ mvn package
~~~

**Trying to run this task on its own will not work.**

9.Profiles
--

**What if we don't want to generate the dist folder with the .jar file every time we build our project?**

How can we run the
goal only when we want to? **Profiles** allow us to create different build configurations.
In this section, we make a simple demonstration of how to create a profile.

There are different scopes for profiles:

- **User Profiles**:
  They are declared in the user's Maven settings file;

- **Global Profiles**:
  They are declared in the global Maven settings file;

- **Project Profiles**:
  They are declared in a projects pom.xml.

**In this demonstration we are interested in the latter.**

It is possible to configure profiles to be activated automatically, in various ways or depending on certain conditions,
such as:

- Maven settings;
- Environment variables;
- Operating System settings;
- Presence or absence of certain files.

We can know which profiles are active at the moment with the following command:

~~~shell
$ mvn help:active-profiles
~~~

It is also possible to call a certain profile from the command line. For instance, profiles can be explicitly specified
using the -P command line flag. The following example:

~~~shell
$ mvn some-phase-here -P some-profile-here
~~~

...is the command we will use to run the package phase under the profile we will create.

### Creating a profile

1. First, we add a new set of tags to our pom.xml(inside the root tags):`<profiles> </profiles>`;
2. Inside it, we nest the `<profile> <profile>` tag set, where we will define our profile;
3. We define the profile's unique id;
4. We create a build section for our profile (`<build> </build>`);
5. At last, we move the plugin we configured previously into the build section of the profile.

**Result:**

~~~xml

<profiles>
    <profile>
        <id>dist</id>
        <build>
            <plugins>
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.0.2</version>
                    <executions>
                        <execution>
                            <id>copy-jar</id>
                            <phase>package</phase>
                            <goals>
                                <goal>copy-resources</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>dist</outputDirectory>
                                <resources>
                                    <resource>
                                        <directory>target</directory>
                                        <includes>
                                            <include>*.jar</include>
                                        </includes>
                                    </resource>
                                </resources>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>

            </plugins>
        </build>
    </profile>
</profiles>
~~~

Now, when we run the package phase with the command:

~~~shell
$ mvn package
~~~

... the dist folder with the .jar file is not generated. To generate the folder, we specify the profile:
~~~shell
$ mvn package -P dist
~~~

**It works!**
______________________
Gradle vs. Maven
===

|   |  Gradle | Maven  |
|---|---|---|
| **Configuration File**  | build.gradle  |  pom.xml |
| **Configuration File language**  | groovy  | xml  |
| **Configuration File Readability**  |  Synthetic, intuitive, understandable. |  Verbose, more difficult to grasp for someone unfamiliarized.  |
| **Build task configuration**  | Has many mechanisms to alter preexisting tasks.  | Done with resource to plugins. Somewhat inflexible: plugin goals are predefined, often bound to phases, order of execution is the order of appearence in pom.xml. In order to automate certain tasks, we need to look for a suitable plugin and get familiar with its documentation to understand how it works and how it is configurable.
|**Task extensibility**|Allows extension of preexisting task types. It is easy to create and run a new task, simply by writing code in the build.gradle file.| Plugins are configurable but not extensible. Creating a plugin is possible but laborious.|
| **Different Build Configurations** | Simply create new tasks, doFirst and doLast may be helpful to achieve this.  |  Different lifecycle configurations are achieved through profiles|
|**Viewing Information About Tasks**|Running the gradle tasks command outputs an organized list of tasks. Related tasks can be grouped. Tasks can have custom names and meaningful descriptions.|Not supported in Maven. Lifecycles and goals have preset names and description is accessible through help commands. |
|  **Ant Integration** | Yes |  Yes |
|  **Daemon** | Yes |  No |

In conclusion, having experimented with both, I found Gradle is easier to learn.
Gradle feels like an upgrade relative to Maven, mainly due to its greater **flexibility** and **briefness**.